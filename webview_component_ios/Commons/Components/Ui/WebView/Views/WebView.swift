//
//  WebView.swift
//  webview_component_ios
//
//  Created by Dani Riders on 16/07/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit
import WebKit
import SnapKit

class WebView: UIView {
    private let webViewConfiguration = WKWebViewConfiguration()
    private weak var delegate: WebViewDelegate?
    
    fileprivate(set) lazy var webViewWK: WKWebView = {
        let webView = WKWebView(frame: .zero, configuration: webViewConfiguration)
        return webView
    }()
    
    
    // MARK: - init
    init(delegate: WebViewDelegate?) {
        self.delegate = delegate
        super.init(frame: .zero)
        setupViewConfiguration()
        setupWebView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup View
    func setup(url:URL,method:HttpMethods = .get,params:[String:String?]?){
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        if let validateParams = params {
            if(method == .post){
                let dataParams = convertToParameters(validateParams)
                request.httpBody = dataParams.data(using: .utf8, allowLossyConversion: true)
            }
        }
        webViewWK.load(request)
        
    }
    
    private func setupWebView() {
        let contentController = WKUserContentController()
        webViewConfiguration.userContentController = contentController
        webViewWK.navigationDelegate = self
        webViewWK.allowsBackForwardNavigationGestures = false
    }
    
    private func convertToParameters(_ params: [String: String?]) -> String {
        var paramList: [String] = []
        
        for (key, value) in params {
            guard let value = value else {continue}
            guard let scapedKey = key.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {continue}
            guard let scapedValue = value.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {continue}
            paramList.append("\(scapedKey)=\(scapedValue)")
        }
        return paramList.joined(separator: "&")
    }
    
}

// MARK: - WKUserContentController
extension WebView: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        // TODO: add messages for actions
        delegate?.didAction(messageAction: message.name + ";" + String(describing: message.body))
    }
}


// MARK: - Delegate WKNavigationDelegate
extension WebView: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        delegate?.didStopLoading()
        
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        if let url = webView.url{
            delegate?.didChangeUrl(url: url)
        }
    }
}

// MARK: - Delegate UIScrollView
extension WebView: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
}


// MARK: - View Configuration
extension WebView: ViewConfiguration {
    
    func configureViews() {
        backgroundColor = .white
        webViewWK.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func buildViewHierarchy() {
        addSubview(webViewWK)
    }
    
    func setupConstraints() {
        
        webViewWK.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(self.safeAreaLayoutGuide.snp.topMargin)
            make.bottom.equalTo(self.safeAreaLayoutGuide.snp.bottomMargin)
        }
        
    }
    
}


