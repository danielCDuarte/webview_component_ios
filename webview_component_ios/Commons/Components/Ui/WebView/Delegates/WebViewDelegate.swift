//
//  WebViewDelegate.swift
//  webview_component_ios
//
//  Created by Dani Riders on 16/07/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import Foundation

protocol WebViewDelegate: class {
    func didAction(messageAction: String)
    func didChangeUrl(url:URL)
    func didStopLoading()
}
