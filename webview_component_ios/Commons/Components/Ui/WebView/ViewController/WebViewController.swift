//
//  WebViewController.swift
//  webview_component_ios
//
//  Created by Dani Riders on 16/07/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit

class WebViewController: UIViewController{
    
    private(set) lazy var webView: WebView = {
        let webView = WebView(delegate: self)
        return webView
    }()
    
    private let url:String
    // MARK: - init
    public init(url:String) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View life cycle
    public override func loadView() {
        super.loadView()
        view = webView
        view.backgroundColor = .white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.webViewWK.scrollView.delegate = self
        self.webView.webViewWK.scrollView.isMultipleTouchEnabled = false
        self.view.showBlurLoader()
        if let url = URL(string: url) {
            //TODO: Add params if is necesary
            self.webView.setup(url: url, params: nil)
        }
        
    }
    
}

// MARK: - Delegate UIScrollView
extension WebViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
}

// MARK: - Delegate LoadWebDelegate
extension WebViewController: WebViewDelegate {
    func didAction(messageAction: String) {
        print("action",messageAction)
    }
    
    func didChangeUrl(url: URL) {
        print("url",url.absoluteString)
    }
    
    func didStopLoading() {
        self.view.removeBluerLoader()
    }
    
}

