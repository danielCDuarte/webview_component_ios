//
//  typeWebViewEnum.swift
//  webview_component_ios
//
//  Created by Dani Riders on 16/07/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import Foundation


enum TypeWebViewEnum: String {
    
    case view = "view"
    case viewController = "viewController"
    
}
