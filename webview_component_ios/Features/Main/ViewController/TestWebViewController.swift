//
//  TestWebViewController.swift
//  webview_component_ios
//
//  Created by Dani Riders on 16/07/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit

class TestWebViewController: UIViewController{
    // MARK: - Properties
    
    private lazy var testWebView: TestWebView = {
        let view = TestWebView(delegate: self)
        return view
    }()
    
    
    private let url:String
    // MARK: - init
    public init(url:String) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View life cycle
    public override func loadView() {
        super.loadView()
        view = testWebView
        view.backgroundColor = .white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.testWebView.webView.webViewWK.scrollView.delegate = self
        self.testWebView.webView.webViewWK.scrollView.isMultipleTouchEnabled = false
        
        self.view.showBlurLoader()
        if let url = URL(string: url) {
            //TODO: Add params if is necesary
            self.testWebView.webView.setup(url: url, params: nil)
        }
        
    }
    
    
}

// MARK: - Delegate UIScrollView
extension TestWebViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
}


extension TestWebViewController: WebViewDelegate{
    func didAction(messageAction: String) {
        print("action",messageAction)
    }
    
    func didChangeUrl(url: URL) {
        print("url",url.absoluteString)
    }
    
    func didStopLoading() {
        self.view.removeBluerLoader()
    }
    
    
}
