//
//  MainViewController.swift
//  webview_component_ios
//
//  Created by Dani Riders on 16/07/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    // MARK: - Properties

    private lazy var mainView: MainView = {
        let view = MainView(delegate: self)
        return view
    }()
    
    // MARK: - Init
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mainView
        // Do any additional setup after loading the view, typically from a nib.
    }

}

extension MainViewController: MainViewDelegate{
    func openWebView(url: String, typeView: TypeWebViewEnum) {
        switch typeView {
        case .view:
            let testWebViewController:TestWebViewController = TestWebViewController(url: url)
            testWebViewController.title = typeView.rawValue
            self.navigationController?.pushViewController(testWebViewController, animated: true)
            break
        case .viewController:
            let webViewController:WebViewController = WebViewController(url: url)
            webViewController.title = typeView.rawValue
            self.navigationController?.pushViewController(webViewController, animated: true)
        }
    }

}
