//
//  TestWebView.swift
//  webview_component_ios
//
//  Created by Dani Riders on 16/07/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit
import SnapKit

class TestWebView: UIView {
    
    // MARK: - Properties
    
    private(set) lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .blue
        return view
    }()
    
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = UIColor.white
        label.font = UIFont(name: "AvenirNext-Medium", size: 16)
        return label
    }()
    
    private(set) lazy var webView: WebView = {
        let webView = WebView(delegate: delegate)
        return webView
    }()
    
    
    // MARK: - init
    private weak var delegate: WebViewDelegate?
    
    init(delegate: WebViewDelegate?) {
        self.delegate = delegate
        super.init(frame: .zero)
        setupViewConfiguration()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - View Configuration

extension TestWebView: ViewConfiguration {
    func configureViews() {
        self.backgroundColor = .white
        self.titleLabel.text = "Contenedor nativo"
    }
    func buildViewHierarchy() {
        
        addSubview(containerView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(webView)
        
    }
    
    func setupConstraints() {
        
        containerView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(self.safeAreaLayoutGuide.snp.topMargin)
            make.bottom.equalTo(self.safeAreaLayoutGuide.snp.bottomMargin)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(32)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
        
        webView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview()
        }
    }
    
}
