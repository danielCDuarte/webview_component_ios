//
//  MainView.swift
//  webview_component_ios
//
//  Created by Dani Riders on 16/07/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit
import SnapKit

class MainView: UIView {

    // MARK: - Properties
    
    private(set) lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 16
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    
    private(set) lazy var vcButton: UIButton = {
        let button = UIButton()
        button.isUserInteractionEnabled = true
        button.setTitle("ViewController", for: .normal)
        button.backgroundColor = UIColor.red
        button.setTitleColor(UIColor.white, for: .normal)
        button.addTarget(self, action: #selector(actionOpenWebViewController(_:)), for: .touchUpInside)
        return button
    }()
    
    private(set) lazy var viewButton: UIButton = {
        let button = UIButton()
        button.isUserInteractionEnabled = true
        button.setTitle("View", for: .normal)
        button.backgroundColor = UIColor.blue
        button.setTitleColor(UIColor.white, for: .normal)
        button.addTarget(self, action: #selector(actionOpenWebView(_:)), for: .touchUpInside)
        return button
    }()
    
    
    // MARK: - init
    private weak var delegate: MainViewDelegate?
    
    init(delegate: MainViewDelegate?) {
        self.delegate = delegate
        super.init(frame: .zero)
        setupViewConfiguration()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Actions
    
    @objc func actionOpenWebView(_ sender : UIButton){
        self.delegate?.openWebView(url: "https://www.google.com/", typeView: .view)
    }
    
    @objc func actionOpenWebViewController(_ sender : UIButton){
        self.delegate?.openWebView(url: "https://www.google.com/", typeView: .viewController)
    }
    
}


// MARK: - View Configuration
extension MainView: ViewConfiguration {
    func configureViews() {
        self.backgroundColor = .white
    }
    func buildViewHierarchy() {
        addSubview(containerView)
        containerView.addSubview(stackView)
        stackView.addArrangedSubview(vcButton)
        stackView.addArrangedSubview(viewButton)
    }
    
    func setupConstraints() {
        
        containerView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(self.safeAreaLayoutGuide.snp.topMargin)
            make.bottom.equalTo(self.safeAreaLayoutGuide.snp.bottomMargin)
        }
        
        stackView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview().offset(-16)
        }
        
        vcButton.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
        }
        
        viewButton.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
        }
    }
    
}

